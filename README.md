# 05 - Use Terraform Module

 This project illustrates how to use a Terraform Module hosted on a GitLab Infrastructure Registry.

 It uses multiple environments which all have their own child-pipelines and are configured
 using Terraform variables, only.